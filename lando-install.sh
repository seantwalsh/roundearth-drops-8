#!/bin/bash

set -e

if [ ! -f .lando.yml ]; then
	cat << EOF > .lando.yml
name: roundearth-drops-8
recipe: pantheon
services:
  appserver:
    build_as_root:
      # Install bower and gulp for Pangea.
      - curl -sL https://deb.nodesource.com/setup_8.x | bash -
      - apt-get install -y nodejs
      - npm install -g bower gulp

      # Install 'cv'
      - 'curl -LsS https://download.civicrm.org/cv/cv.phar -o /usr/local/bin/cv'
      - 'chmod +x /usr/local/bin/cv'

      # Install 'civix'
      - 'curl -LsS https://download.civicrm.org/civix/civix.phar -o /usr/local/bin/civix'
      - 'chmod +x /usr/local/bin/civix'
config:
  framework: drupal8
  drush: '8.1.18'
tooling:
  drush:
    service: appserver
    cmd: drush --root=/app/web"
  drupal:
    service: appserver
  npm:
    service: appserver
  cv:
    service: appserver
  civix:
    service: appserver
EOF
fi

lando start

# The install always fails because of the sendmail_path, so, for now, we'll
# just carry on even if there's a failure.
#
# See: https://github.com/lando/lando/issues/756
#
set +e

lando drush si --account-mail=roundearth@exmaple.com --account-name=admin --account-pass=admin --site-mail=roundearth@example.com --site-name="Round Earth" roundearth -y

# ... and then switch back.
set -e

# Restore the civicrm.settings.php, because the installer overwrites it.
chmod +w web/sites/default
git checkout -- web/sites/default/civicrm.settings.php
chmod -w web/sites/default

lando drush cc drush
lando drush cvapi Setting.create assetCache=0 
lando drush cvapi system.flush
lando drush cr

echo "Username: admin"
echo "Password: admin"

