(function(angular, $, _){
  angular.module('postal', []);

  // Override template if attachments are disabled.
  if (!CRM.postal.postal_allow_attachments) {
    angular.module('crmAttachment').config(function ($provide) {
      $provide.decorator('crmAttachmentsDirective', function ($delegate) {
        $delegate[0].template = '<div ng-if="ready">Attachments disabled.</div>';
        return $delegate;
      });
    });
  }
})(angular, CRM.$, CRM._);
