<?php

namespace Drupal\roundearth_events\Menu;

use Drupal\Core\Menu\LocalTaskDefault;
use Drupal\Core\Routing\RouteMatchInterface;

/**
 * Local task plugin class for CiviCRM event.
 */
class EventsDrupalLocalTask extends LocalTaskDefault {

  /**
   * {@inheritdoc}
   */
  public function getRouteParameters(RouteMatchInterface $route_match) {
    if (! isset($_GET['id'])) {
      return [];
    }

    $parameters = [
      'civicrm_event' => $_GET['id'],
    ];
    return $parameters;
  }
}
