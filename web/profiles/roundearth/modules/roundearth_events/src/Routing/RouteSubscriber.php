<?php

namespace Drupal\roundearth_events\Routing;

use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\RouteCollection;

/**
 * Route subscriber for altering some routes.
 */
class RouteSubscriber extends RouteSubscriberBase {

  /**
   * {@inheritdoc}
   */
  protected function alterRoutes(RouteCollection $collection) {
    if ($route = $collection->get('civicrm.civicrm_event_manage_settings')) {
      $route->setRequirements([
        '_permission' => 'edit all events+edit own events',
      ]);
    }
  }

}
