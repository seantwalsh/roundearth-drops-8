<?php

namespace Drupal\roundearth_migration\CiviCRM;

trait CiviCrmAwareTrait {

  /**
   * Gets the CiviCRM service.
   *
   * @return \Drupal\civicrm\Civicrm
   *   The CiviCRM service.
   */
  protected function getCivi() {
    return \Drupal::service('civicrm');
  }

}
