<?php

namespace Drupal\roundearth_migration;

use Drupal\Component\Utility\NestedArray;

trait ConfigAwareTrait {

  /**
   * Gets the configuration.
   *
   * Merges the default configuration, the module configuration, and the plugin
   * configuration.
   *
   * @return array
   *   The configuration.
   */
  protected function getConfiguration() {
    $config = NestedArray::mergeDeep($this->defaultConfiguration, $this->getSettings()->get(), $this->configuration);

    return $config;
  }

  /**
   * Gets the Roundearth Migration configuration.
   *
   * @return \Drupal\Core\Config\ImmutableConfig
   *   Roundearth Migration configuration.
   */
  protected function getSettings() {
    return \Drupal::config('roundearth_migration.settings');
  }

}
