<?php

namespace Drupal\roundearth_migration\Plugin\migrate\process;

use DOMDocument;
use DOMElement;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\file\Entity\File;
use Drupal\media\Entity\Media;
use Drupal\media\MediaInterface;
use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\Plugin\MigrationInterface;
use Drupal\migrate\Row;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class EmbeddedMedia.
 *
 * Converts files and images found within text to media entities and embeds them
 * within the WYSIWYG text.
 *
 * @MigrateProcessPlugin(
 *   id = "roundearth_migration_embedded_media"
 * )
 */
class EmbeddedMedia extends UrlProcessorBase implements ContainerFactoryPluginInterface {

  /**
   * Default configuration values.
   *
   * @var array
   */
  protected $defaultConfig = [
    'base_path',
    'files_path',
    'hosts',
    'video_tokens',
    'files_source',
  ];

  /**
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $settings;

  /**
   * EmbeddedMedia constructor.
   *
   * @param array $configuration
   * @param string $plugin_id
   * @param mixed $plugin_definition
   * @param \Drupal\Core\Config\ImmutableConfig $settings
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, ImmutableConfig $settings) {
    $this->settings = $settings;
    parent::__construct($configuration, $plugin_id, $plugin_definition);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('config.factory')->get('roundearth_migration.settings')
    );
  }

  /**
   * Get plugin configuration.
   *
   * @return array
   *   Configuration values.
   */
  protected function getConfig() {
    $config = $this->configuration;
    foreach ($this->defaultConfig as $key) {
      if (empty($config[$key])) {
        $config[$key] = $this->settings->get($key);
      }
    }
    return $config;
  }

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    $text = parent::transform($value, $migrate_executable, $row, $destination_property);

    if ($this->getConfig()['video_tokens']) {
      $text = $this->processVideoTokens($text);
    }

    return $text;
  }

  /**
   * {@inheritdoc}
   */
  protected function setUrl(DOMElement $element, $attribute, $value, $original) {
    switch ($element->tagName) {
      case 'a':
        // Skip if contains an image.
        if ($element->getElementsByTagName('img')) {
          return;
        }
        $media = $this->findMediaEntity($value, 'file');
        $element->parentNode->replaceChild($this->getMediaEmbed($media, $element->ownerDocument), $element);
        $message = sprintf('Link with href="%s" converted to media ID %d.', $original, $media->id());
        $this->log($message, MigrationInterface::MESSAGE_INFORMATIONAL);
        break;

      case 'img':
        $options = $this->getImageOptions($element);
        $media = $this->findMediaEntity($value, 'image');
        $element->parentNode->replaceChild($this->getMediaEmbed($media, $element->ownerDocument, $options), $element);
        $message = sprintf('Image with href="%s" converted to media ID %d.', $original, $media->id());
        $this->log($message, MigrationInterface::MESSAGE_INFORMATIONAL);
        break;
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function deferSetUrl(DOMElement $element, $value) {
    switch ($element->tagName) {
      case 'a':
        $message = sprintf('Link with href="%s" not a media link, skipping.', $value);
        $this->log($message, MigrationInterface::MESSAGE_INFORMATIONAL);
        break;

      case 'img':
        $message = sprintf('Image with src="%s" not a media image, skipping.', $value);
        $this->log($message, MigrationInterface::MESSAGE_INFORMATIONAL);
        break;
    }
  }

  /**
   * Get the URI to the media file.
   *
   * @param string $url
   *   The URL.
   *
   * @return string|false
   *   The URI, or false if the URL is not a local file.
   */
  protected function processUrl($url) {
    if (!$url = $this->internalUrl($url)) {
      $message = sprintf('URL "%s" does not have an internal path.', $url);
      $this->log($message, MigrationInterface::MESSAGE_INFORMATIONAL);
      return FALSE;
    }

    // Drop everything except the path.
    $parts = parse_url($url);
    $path = $parts['path'];

    // Find the prefix to the uploaded files.
    $config = $this->getConfig();
    $filesPrefix = '/' . trim($config['files_path'], '/') . '/';

    // Bail if path is not for an uploaded file.
    $length = strlen($filesPrefix);
    if (substr($path, 0, $length) != $filesPrefix) {
      $message = sprintf('Path "%s" is not an uploaded file.', $path);
      $this->log($message, MigrationInterface::MESSAGE_INFORMATIONAL);
      return FALSE;
    }

    // Convert the path to a URI.
    $uri = 'public://' . substr($path, $length);

    // If the file does not exist, attempt to copy it from source files.
    if (!file_exists($uri)) {
      if (!$this->obtainSourceFile($uri)) {
        $message = sprintf('File for URI "%s" does not exist.', $uri);
        $this->log($message, MigrationInterface::MESSAGE_INFORMATIONAL);
        return FALSE;
      }
    }

    return $uri;
  }

  /**
   * Attempt to get the source file for a given URI.
   *
   * @param string $uri
   *   The URI of the file we're attempting to obtain the source file for.
   *
   * @return bool
   *   Indicates if we were able to obtain the source file.
   */
  protected function obtainSourceFile($uri) {
    $config = $this->getConfig();

    if (empty($config['files_source'])) {
      return FALSE;
    }
    $filesSource = trim($config['files_source'], '/');

    if (empty($config['files_path'])) {
      return FALSE;
    }
    $filesPath = trim($config['files_path'], '/');

    // Get the source URI.
    $sourcePrefix = $filesSource . '/' . $filesPath;
    $length = strlen('public://');
    $sourceUri = $sourcePrefix . '/' . substr($uri, $length);

    $dir = $this->getFileSystem()->dirname($uri);
    file_prepare_directory($dir, FILE_CREATE_DIRECTORY | FILE_MODIFY_PERMISSIONS);

    // Copy file.
    if (!file_unmanaged_copy($sourceUri, $uri, FILE_EXISTS_ERROR)) {
      // If it fails, attempt to copy with decoded URI.
      $sourceUri = urldecode($sourceUri);
      if (!file_unmanaged_copy($sourceUri, $uri, FILE_EXISTS_ERROR)) {
        return FALSE;
      }
    }

    return TRUE;
  }

  /**
   * Gets or creates the file for a given URI.
   *
   * @param string $uri
   *   The file URI.
   *
   * @return \Drupal\file\FileInterface
   *   The file.
   */
  protected function findFile($uri) {
    $fids = \Drupal::entityQuery('file')
      ->condition('uri', $uri)
      ->execute();

    // Grab the first file ID and attempt to load it.
    $file = NULL;
    if ($fid = reset($fids)) {
      $file = File::load($fid);
    }

    // If we still don't have the file, whip up a new one.
    if (!$file) {
      $file = File::create([
        'uri' => $uri,
        'status' => FILE_STATUS_PERMANENT,
        'uid' => 1,
        ]);
      $file->save();
    }
    return $file;
  }

  /**
   * Find the media entity for a specified URL.
   *
   * @param string $uri
   *   The URI to the media's file.
   * @param string $type
   *   The media type/bundle.
   *
   * @return \Drupal\media\MediaInterface|null
   *   The media entity, or NULL if none found.
   */
  protected function findMediaEntity($uri, $type) {
    $file = $this->findFile($uri);

    // Entity query to obtain the media entity by the file's URI.
    $field = 'field_media_' . $type;
    $queryField = $field . '.entity.uri';
    $mids = \Drupal::entityQuery('media')
      ->condition($queryField, $file->getFileUri())
      ->execute();

    // Grab the first media ID and attempt to load it.
    $media = NULL;
    if ($mid = reset($mids)) {
      $media = Media::load($mid);
    }

    // If we still don't have the media, whip up a new one.
    if (!$media) {
      $media = Media::create([
        'bundle' => $type,
        $field => ['target_id' => $file->id()],
      ]);
      $media->save();
    }

    return $media;
  }

  /**
   * Produces the media embed.
   *
   * @param \Drupal\media\MediaInterface $media
   *   The media entity.
   * @param \DOMDocument $document
   *   The document.
   * @param array $options
   *   Optional, additional options for the embed.
   *
   * @return \DOMElement
   *   The embed element.
   */
  protected function getMediaEmbed(MediaInterface $media, DOMDocument $document, array $options = []) {
    $embed = $document->createElement('drupal-entity');
    $embed->setAttribute('data-embed-button', 'panopoly_media_wysiwyg_media_embed');
    $embed->setAttribute('data-entity-embed-display', 'view_mode:media.embed_large');
    $embed->setAttribute('data-entity-type', 'media');
    $embed->setAttribute('data-entity-uuid', $media->uuid());

    if (!empty($options['align'])) {
      switch ($options['align']) {
        case 'left':
        case 'right':
        $embed->setAttribute('data-align', $options['align']);
      }
    }

    if (!empty($options['display'])) {
      $embed->setAttribute('data-entity-embed-display', 'view_mode:media.' . $options['display']);
    }
    else {
      $embed->setAttribute('data-entity-embed-display', 'view_mode:media.embed_large');
    }

    return $embed;
  }

  /**
   * Gets emebed options from a source image element.
   *
   * @param \DOMElement $image
   *   The image.
   *
   * @return array
   *   Options.
   */
  protected function getImageOptions($image) {
    $options = [];

    // Explode the styles value into structured data.
    $style = $image->getAttribute('style');
    $stylesRaw = array_filter(explode(';', $style));
    $styles = [];
    foreach ($stylesRaw as $style) {
      $parts = array_map('trim', explode(':', $style, 2));
      if (count($parts) == 2) {
        $styles[$parts[0]] = $parts[1];
      }
    }

    if (!empty($styles['float'])) {
      $options['align'] = $styles['float'];
    }

    if ($align = $image->getAttribute('align')) {
      $options['align'] = $align;
    }

    if (!empty($options['align'])) {
      $options['display'] = 'embed_medium';
    }

    return $options;
  }

  /**
   * Convert video tokens into embedded media.
   *
   * @param string $text
   *   The text with tokens.
   *
   * @return string
   *   The value with the tokens replaced with media embeds.
   */
  protected function processVideoTokens($text) {
    preg_match_all('/\[video(\:(.+))?( .+)?\]/isU', $text, $matches_code);
    foreach ($matches_code[0] as $ci => $code) {
      if (!$media = $this->getVideoMedia($matches_code[2][$ci])) {
        continue;
      }

      $document = new DOMDocument();
      $document->appendChild($this->getMediaEmbed($media, $document));
      $replacement = $document->saveHTML();
      $text = str_replace($code, $replacement, $text);
    }
    return $text;
  }

  /**
   * Gets or creates a video media entity for a given URL.
   *
   * @param string $url
   *   The video URL.
   *
   * @return MediaInterface|null
   *   The video media.
   */
  protected function getVideoMedia($url) {
    /** @var \Drupal\video_embed_field\ProviderManager $providerManager */
    $providerManager = \Drupal::service('video_embed_field.provider_manager');
    /** @var \Drupal\video_embed_field\ProviderPluginInterface $provider */
    $provider = $providerManager->loadProviderFromInput($url);
    if (!$provider) {
      return;
    }

    $videoId = $provider->getIdFromInput($url);

    // Load all videos that have the ID in the URL.
    $mids = \Drupal::entityQuery('media')
      ->condition('bundle', 'video')
      ->condition('field_media_video_embed_field', $videoId, 'CONTAINS')
      ->execute();

    $videos = Media::loadMultiple($mids);
    foreach ($videos as $video) {
      /** @var \Drupal\video_embed_field\ProviderPluginInterface $videoProvider */
      $videoProvider = $providerManager->loadProviderFromInput($video->field_media_video_embed_field->value);
      if ($provider->getPluginId() != $videoProvider->getPluginId()) {
        continue;
      }
      if ($provider->getIdFromInput($video->field_media_video_embed_field->value) == $videoId) {
        return $video;
      }
    }

    // None found, create one.
    $media = Media::create([
      'name' => $provider->getName(),
      'bundle' => 'video',
      'field_media_video_embed_field' => $url,
    ]);
    $media->save();
    return $media;
  }

  /**
   * Logs a message on the current migration.
   *
   * @param string $message
   *   The message.
   * @param int $level
   *   The message level, defaults to ERROR.
   */
  protected function log($message, $level = MigrationInterface::MESSAGE_ERROR) {
    if ($this->migrateExecutable) {
      $this->migrateExecutable->saveMessage($message, $level);
    }
  }

  /**
   * Get the file system service.
   *
   * @return \Drupal\Core\File\FileSystemInterface
   *   The file system service.
   */
  protected function getFileSystem() {
    return \Drupal::service('file_system');
  }

}
