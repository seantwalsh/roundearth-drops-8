<?php

namespace Drupal\roundearth_migration\Plugin\migrate\process;

use DOMDocument;
use Drupal\Component\Utility\Html;
use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\Row;

/**
 * Class TagReplace.
 *
 * Replaces specified tags in markup.
 *
 * @MigrateProcessPlugin(
 *   id = "roundearth_migration_tag_replace"
 * )
 */
class TagReplace extends ProcessPluginBase {

  /**
   * @var array
   */
  protected $defaultConfig = [
    'map' => ['b' => 'strong', 'i' => 'em']
  ];

  /**
   * @return array
   */
  protected function getConfig() {
    $config = $this->configuration;

    foreach ($this->defaultConfig as $key => $value) {
      if (!isset($config[$key])) {
        $config[$key] = $value;
      }
    }

    return $config;
  }

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    $changed = FALSE;
    $document = Html::load($value);

    foreach ($this->getConfig()['map'] as $origTag => $newTag) {
      if ($this->replaceTag($document, $origTag, $newTag)) {
        $changed = TRUE;
      }
    }

    return $changed ? Html::serialize($document) : $value;
  }

  /**
   * @param \DOMDocument $document
   * @param string $origTag
   * @param string $newTag
   * @return bool
   */
  protected function replaceTag(DOMDocument $document, $origTag, $newTag) {
    $changed = FALSE;

    /** @var \DOMNodeList $els */
    $els = $document->getElementsByTagName($origTag);

    foreach (iterator_to_array($els) as $el) {
      /** @var \DOMElement $el */
      $newNode = $document->createElement($newTag);

      // Copy children.
      while ($el->firstChild) {
        $newNode->appendChild($el->firstChild);
      }

      // Copy attributes.
      foreach ($el->attributes as $attrName => $attrNode) {
        $newNode->setAttribute($attrName, $attrNode->nodeValue);
      }

      $el->parentNode->replaceChild($newNode, $el);
      $changed = TRUE;
    }

    return $changed;
  }

}
