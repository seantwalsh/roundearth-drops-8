<?php

namespace Drupal\roundearth_migration\Plugin\migrate\source;

use Drupal\migrate\Plugin\migrate\source\SourcePluginBase;
use Drupal\migrate\Row;
use FilesystemIterator;
use RecursiveDirectoryIterator;
use RecursiveIteratorIterator;

/**
 * Class Files.
 *
 * Configuration options:
 *
 *   - base_path: Where to search for the files.
 *   - recursive: Whether or not to recurse into subdirectories to find files.
 *   - include_dirs: Whether or not to include directories.
 *
 * @MigrateSource(
 *   id = "roundearth_files",
 *   source_module = "roundearth_migration"
 * )
 */
class Files extends SourcePluginBase {

  /**
   * {@inheritdoc}
   */
  public function fields() {
    return [
      'file_name' => $this->t('File name'),
      'full_path' => $this->t('Full path to the file'),
      'full_path_name' => $this->t('Full path to the file including the file name.'),
      'path' => $this->t('File path not including the base path.'),
      'path_name' => $this->t('File path including the file name.'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function __toString() {
    return __CLASS__;
  }

  /**
   * {@inheritdoc}
   */
  public function getIds() {
    return [
      'full_path_name' => [
        'type' => 'text',
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function initializeIterator() {
    if (!empty($this->configuration['recursive'])) {
      $d = new RecursiveDirectoryIterator($this->configuration['base_path'], FilesystemIterator::SKIP_DOTS);
      $i = new RecursiveIteratorIterator($d, RecursiveIteratorIterator::SELF_FIRST);
    }
    else {
      $i = new FilesystemIterator($this->configuration['base_path'], FilesystemIterator::SKIP_DOTS);
    }

    // Exclude directories per config.
    $f = new \CallbackFilterIterator($i, function ($item) {
      /** @var \SplFileInfo $item */
      if (empty($this->configuration['include_dirs']) && $item->isDir()) {
        return FALSE;
      }

      return TRUE;
    });

    // Convert to array so we can extract the data we need.
    $rows = array_map(function ($item) {
      /** @var \SplFileInfo $item */
      $path = ltrim(substr($item->getPath(), strlen($this->configuration['base_path'])), '/');
      return [
        'file_name' => $item->getFilename(),
        'full_path' => $item->getPath(),
        'full_path_name' => $item->getPathname(),
        'path' => $path,
        'path_name' => ltrim($path . '/' . $item->getFilename(), '/'),
      ];
    }, iterator_to_array($f));

    // Turn back to an iterator, and use it.
    return new \ArrayIterator($rows);
  }

  /**
   * {@inheritdoc}
   */
  public function prepareRow(Row $row) {
    return parent::prepareRow($row);
  }

}
