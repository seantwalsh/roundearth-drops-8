<?php
/**
 * @file
 * Theme and preprocess functions for fields.
 */

/**
 * Implements template_preprocess_field_collection_item().
 */
function pangea_preprocess_field_collection_item(&$variables) {
  // Add the image to the template.
  $item = $variables['item']['#field_collection_item'];
  if (($item->hasField('field_roundearth_carousel_media')) && ($media = $item->field_roundearth_carousel_media->entity)) {
    /** @var \Drupal\file\FileInterface $file */
    if (($media->hasField('field_media_image')) && ($file = $media->field_media_image->entity)) {
      $variables['file_url'] = file_create_url($file->getFileUri());
    }
  }
}

/**
 * Implements template_preprocess_field().
 */
function pangea_preprocess_field(&$variables) {
  $field_name = $variables['field_name'];

  switch ($field_name) {
    case 'field_roundearth_events_locphoto':
      // Find the original image path for each item.
      foreach ($variables['items'] as $key => $item) {
        /** @var \Drupal\image\Plugin\Field\FieldType\ImageItem $image_item */
        $image_item = $item['content']['#item'];
        $media = $image_item->getEntity();

        // Add the full image url.
        if (($media->hasField('field_media_image')) && ($files = $media->get('field_media_image')->referencedEntities())) {
          /** @var \Drupal\file\FileInterface $file */
          $file = reset($files);
          $variables['items'][$key]['url'] = file_create_url($file->getFileUri());
        }

        // Add the description for caption.
        if ($media->hasField('field_panopoly_media_description')) {
          $variables['items'][$key]['description'] = [
            '#markup' => $media->get('field_panopoly_media_description')->value,
          ];
        }
      }
      break;
  }
}
