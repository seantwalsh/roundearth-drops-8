<?php

/**
 * Implements template_preprocess_page();
 */
function pangea_preprocess_page(&$variables) {
  // Replace [YEAR] with the current year and add copyright to template.
  $copyright = theme_get_setting('copyright');
  $variables['copyright'] = str_replace('[YEAR]', date('Y'), $copyright);

  $container = theme_get_setting('container');

  // Set default value for container to handle existing sites.
  if (empty($container)) {
    $container = 'fixed';
  }

  // Attach library.
  $variables['#attached']['library'][] = 'pangea/' . $container;
}

/**
 * Implements hook_theme_suggestions_alter().
 */
function pangea_theme_suggestions_alter(array &$suggestions, array $variables) {
  $container = theme_get_setting('container');

  // Set default value for container to handle existing sites.
  if (empty($container)) {
    $container = 'fixed';
  }

  // Add original hook to suggestions.
  array_unshift($suggestions, $variables['theme_hook_original']);

  // Add a suggestion based on the container setting.
  foreach ($suggestions as $suggestion) {
    $suggestions[] = $suggestion . '__' . $container;
  }
}
