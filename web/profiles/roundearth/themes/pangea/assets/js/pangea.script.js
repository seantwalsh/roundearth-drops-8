/**
 * @file
 * Custom scripts for theme.
 */
(function ($) {

  // JS fixes for civiCRM contrib forms.
  Drupal.behaviors.civiCRMContribForm = {
    attach: function (context) {
      var $block = $('#crm-container.crm-public div.crm-contribution-main-form-block', context);
      $block.find('.crm-section, .crm-public-form-item, .label').each(function () {
        // If a div is empty with spaces, make is empty so that :empty selector picks it.
        var html = $(this).html().trim().replace('&nbsp;', '');
        if (html.length === 0) {
          $(this).html('');
        }
      });
    }
  };

  // Hide carousel until first image is loaded and ready.
  Drupal.behaviors.carouselImagesLoaded = {
    attach: function (context) {
      var $carousels = $('.carousel', context);

      if ($carousels.length) {
        $carousels.each(function () {
          var $carousel = $(this);
          $carousel.find('.carousel-inner .item:first-child').imagesLoaded( function() {
            $carousel.addClass('loaded');
          });
        });
      }
    }
  };

  // Handle the user mobile menu toggle.
  Drupal.behaviors.mobileMenuToggle = {
    attach: function (context) {
      // Show the mobile menu toggle if the user menu is in navbar.
      if ($('.navbar-collapse--user .menu--account', context).length) {
        $('.navbar-toggle.user-menu', context).removeClass('hidden');
      }
    }
  };

  Drupal.behaviors.menuTree = {
    attach: function (context) {
      var $menuTree = $('.menu--tree', context);
      $menuTree.find('.btn-toggle').click(function (event) {
        event.preventDefault();
        $(this).next('ul').slideToggle();
        $(this).parent('li').toggleClass('open');
      });

      $menuTree.find('a.is-active').parent('.menu-item--expanded').addClass('open');
    }
  }

  // CiviCRM Contact
  Drupal.behaviors.civicrmContact = {
    attach: function (context) {
      $('.contact__bio__link', context).click(function (event) {
        event.preventDefault();
        $(this).parents('.contact').find('.contact__bio__content').slideToggle();
        $(this).find('i').toggleClass('fa-plus-circle fa-minus-circle');
      });
    }
  };

})(jQuery);
