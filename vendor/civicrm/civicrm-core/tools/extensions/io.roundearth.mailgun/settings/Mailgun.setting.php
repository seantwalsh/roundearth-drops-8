<?php

return [
  'mailgun_webhook_signing_key' => [
    'group_name' => 'Mailgun',
    'group' => 'mailgun',
    'name' => 'mailgun_webhoo_signing_key',
    'type' => 'String',
    'default' => '',
    'add' => '4.3',
    'is_domain' => 1,
    'is_contact' => 0,
    'title' => 'Mailgun Webhook signing key',
    'description' => 'Your Mailgun Webhook signing key used to verify requests to the CiviCRM webhook. Without this, anyone on the internet can POST to your webhook without authentication!',
    'help_text' => '',
    'quick_form_type' => 'Element',
    'html_type' => 'text',
    'html_attributes' => [
      'size' => 50,
    ],
  ],
];
