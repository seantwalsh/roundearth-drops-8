# io.roundearth.mailgun

![Screenshot](/images/screenshot.png)

Provides CiviCRM integration with [Mailgun](https://www.mailgun.com).

In order to access the Mailgun API (needed by the CiviMail integration),
you'll need to install the PHP library via composer:

```$bash
composer require mailgun/mailgun

```

The extension is licensed under [AGPL-3.0](LICENSE.txt).

## Requirements

* PHP v5.4+
* CiviCRM 4.7+

## Installation (Web UI)

This extension has not yet been published for installation via the web UI.

## Installation (CLI, Zip)

Sysadmins and developers may download the `.zip` file for this extension and
install it with the command-line tool [cv](https://github.com/civicrm/cv).

```bash
cd <extension-dir>
cv dl io.roundearth.mailgun@https://gitlab.com/roundearth/io.roundearth.mailgun/repository/archive.zip?ref=master
```

## Installation (CLI, Git)

Sysadmins and developers may clone the [Git](https://en.wikipedia.org/wiki/Git) repo for this extension and
install it with the command-line tool [cv](https://github.com/civicrm/cv).

```bash
git clone https://gitlab.com/roundearth/io.roundearth.mailgun.git
cv en mailgun
```

## Usage

(* FIXME: Where would a new user navigate to get started? What changes would they see? *)

## Known Issues

(* FIXME *)

## TODO

- ???

