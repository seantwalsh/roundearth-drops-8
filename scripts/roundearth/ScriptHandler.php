<?php

/**
 * @file
 * Contains \DrupalProject\composer\ScriptHandler.
 */

namespace DrupalProject\roundearth;

use Composer\IO\IOInterface;
use Composer\Script\Event;
use Symfony\Component\Process\Process;

class ScriptHandler {

  protected static function runProcess($cmd, $path, IOInterface $io) {
    $process = new Process($cmd, $path);
    $process->setTimeout(NULL);
    $process->mustRun();

    $io->write($process->getOutput(), FALSE, IOInterface::VERBOSE);
  }

  public static function buildPangeaTheme(Event $event) {
    $io = $event->getIO();
    $pangea_path = './web/profiles/roundearth/themes/pangea';

    if (!file_exists("{$pangea_path}/bower_components")) {
      $io->write("  - <info>Running bower for Pangea theme...</info>");
      static::runProcess("bower install", $pangea_path, $io);
    }

    if (!file_exists("{$pangea_path}/node_modules")) {
      $io->write("  - <info>Running npm for Pangea theme...</info>");
      static::runProcess("npm install", $pangea_path, $io);
    }

    $io->write("  - <info>Running gulp for Pangea theme...</info>");
    static::runProcess("gulp css", $pangea_path, $io);
  }

  public static function getVersafixTemplate(Event $event) {
      $io = $event->getIO();
      $packages_path = "./web/libraries/civicrm/tools/extensions/uk.co.vedaconsulting.mosaico/packages/mosaico/templates/versafix-1/";
      $new_template_path = "https://raw.githubusercontent.com/mfrausto-mdw/mosaico/replaced-title-template/templates/versafix-1";

      if (file_exists("{$packages_path}/template-versafix-1.html")){
          //Create a backup before getting the new template
          if (file_exists("{$packages_path}/template-versafix-1.html.old")) {
              static::runProcess("rm template-versafix-1.html", $packages_path, $io);
          } else {
              static::runProcess("mv template-versafix-1.html template-versafix-1.html.old", $packages_path, $io);
          }
      }

      static::runProcess("wget {$new_template_path}/template-versafix-1.html", $packages_path, $io);
  }

}
